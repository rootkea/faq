<chapt id="software">Software disponibile nel sistema Debian

<sect id="apps">Quali tipi di applicazioni e software di sviluppo sono disponibili per &debian;?

<p>Come molte distribuzioni Linux, &debian; fornisce:
<list>
  <item>le maggiori applicazioni GNU per lo sviluppo del software,
    manipolazione dei file ed elaborazione testi, inclusi gcc, g++,
    make, texinfo, Emacs, la shell Bash e numerose utilit� Unix
    aggiornate,
  <item>Perl, Python, Tcl/Tk e vari programmi correlati, moduli e librerie
    per ognuno di essi,
  <item>TeX (LaTeX) e Lyx, dvips, Ghostscript,
  <item>il Sistema X Window, che fornisce un'interfaccia grafica
    orientata alla rete per Linux ed innumerevoli applicazioni per X
    incluso GNOME,
  <item>una completa suite di applicazioni per la rete, inclusi server
    per protocolli Internet come HTTP (WWW), FTP, NNTP (news), SMTP e
    POP (mail) e name server; fornisce anche browser web e strumenti per lo
    sviluppo.
</list>

<p>Sono inclusi nella distribuzione pi� di &main-pkgs; pacchetti che spaziano
dai server e lettori per le news al supporto per il suono, programmi per
FAX, programmi per database e fogli di calcolo, programmi per
l'elaborazione delle immagini, comunicazioni, rete e utilit� per la
posta elettronica, server Web e persino programmi ham-radio. Altre
&contrib-nonfree-pkgs; suite di software sono disponibili come pacchetti Debian,
ma non fanno formalmente parte di Debian a causa di restrizioni delle licenze.

<sect id="softwareauthors">Chi ha scritto tutto questo software?

<p>Per ogni pacchetto gli <em>autori</em> del programma (o dei programmi) sono
menzionati nel file <tt>/usr/doc/PACKAGE/copyright</tt>, dove PACKAGE � da
sostituire con il nome del pacchetto.

<p>I <em>Manutentori</em> che impacchettano questo software per il sistema &debian;
sono elencati nel file di controllo Debian (si veda <ref id="controlfile">) che
� fornito con ogni pacchetto.

<sect id="pkglist">Come si pu� ottenere una lista aggiornata dei programmi che sono stati
  impacchettati per Debian?

<p>Una lista completa � disponibile in due parti:
<taglist>
  <tag>la lista dei pacchetti che possono essere distribuiti ovunque
     <item>da ognuno dei <url name="mirror Debian"
           id="http://www.debian.org/distrib/ftplist">, nel file
           <tt>indices/Maintainers</tt>.
           Quel file contiene i nomi dei pacchetti e i nomi e le e-mail dei
           rispettivi manutentori.
  <tag>la lista dei pacchetti che non possono essere esportati dagli Stati Uniti
     <item>da ognuno dei <url name="mirror non-US Debian"
           id="http://www.debian.org/misc/README.non-US">, nel file
           <tt>indices-non-US/Maintainers</tt>.
           Quel file contiene i nomi dei pacchetti e i nomi e le e-mail dei
           rispettivi manutentori.
</taglist>

<p>L'<url name="interfaccia WWW ai pacchetti Debian"
id="http://packages.debian.org/"> riassume convenientemente i
pacchetti in ognuna delle circa venti "sezioni" dell'archivio Debian.

<sect id="missing">Cosa manca da &debian;?

<p>Esiste una lista di pacchetti che sono ancora da impacchettare per
Debian, la <url id="http://www.debian.org/devel/wnpp/" name="Work-Needing
and Prospective Packages list">.

<p>Per maggiori dettagli sull'aggiunta di cose mancanti, si veda <ref id="contrib">.

<sect id="no-devs">Perch� ricevo il messaggio "ld: cannot find -lfoo" quando compilo
  i programmi? Perch� non ci sono i file libfoo.so nei pacchetto delle librerie Debian?

<p>La Debian Policy richiede che un tale link simbolico (a
libfoo.so.x.y.z o simile) sia posto in un pacchetto di sviluppo
separato. Questi pacchetti di solito sono chiamati libfoo-dev o
libfooX-dev (presumendo che il pacchetto libreria si chiami libfooX, e X
sia un numero intero).

<sect id="java">Debian supporta Java? (E come?)

<p>Poich� il Java Development kit ufficiale della Sun Microsystems �
software non-libero, non pu� essere incluso nella Debian vera e propria. Comunque, sia
il JDK che diverse implementazioni <em>libere</em> della tecnologia Java sono
disponibili come pacchetti Debian. Si possono scrivere, fare debug ed eseguire programmi Java
usando Debian.

<p>Il funzionamento di applet Java richiede un browser web con
la capacit� di riconoscerle ed eseguirle. Diversi browser web disponibili
in Debian, come Mozilla o Konqueror, supportano i plug-in Java che abilitano l'esecuzione
di applet Java. Netscape Navigator, sebbene non-libero, � comunque disponibile
come pacchetto Debian e pu� eseguire applet Java,

<p>Si faccia riferimento alla <url name="Java FAQ Debian"
id="http://www.debian.org/doc/manuals/debian-java-faq/"> per maggiori informazioni.

<sect id="isitdebian">Come si pu� verificare che si sta usando un sistema Debian e che
  versione �?

<p>Per assicurarsi che il proprio sistema sia stato installato da una
reale distribuzione di dischi di base Debian si verifichi l'esistenza
del file <tt>/etc/debian_version</tt>, che contiene una singola riga che fornisce
il numero di versione della release, come definito dal pacchetto <tt>base-files</tt>.

<p>L'esistenza del programma <tt>dpkg</tt> mostra che si dovrebbe essere in grado di
installare pacchetti Debian sul proprio sistema, ma siccome il programma �
stato portato su molti altri sistemi operativi ed architetture,
non � pi� un metodo fidato per determinare se � un sistema &debian;.

<p>Gli utenti dovrebbero comunque essere consci che il sistema Debian consiste
di molte parti, ognuna delle quali pu� essere aggiornata (quasi)
indipendentemente. Ogni "release" Debian � formata da un contenuto
ben definito e invariato. Gli aggiornamenti sono disponibili separatamente.
Per una descrizione su un'unica riga dello stato di installazione del pacchetto
<tt>foo</tt>, si usi il comando <tt>dpkg --list foo</tt>.
Per vedere la versione di tutti i pacchetti installati, si esegua:
  <example>dpkg -l</example>
Per una descrizione pi� accurata, si usi:
  <example>dpkg --status foo</example>

<sect id="nonenglish">Come fa Debian a supportare lingue non-inglesi?

<p><list>
  <item>&debian; � distribuita con keymap per almeno due dozzine
    di tastiere e con utilit� (nel pacchetto <tt>kbd</tt>)
    per installare, vedere e modificare le tabelle dei caratteri.
    <p>L'installazione chieder� all'utente di specificare la tastiera che user�.
  <item>Una vasta maggioranza del software impacchettato supporta interamente
    i caratteri non-US-ASCII usati in altri linguaggi Latin (p.e.
    ISO-8859-1 o ISO-8859-2) ed un certo numero di programmi supporta
    linguaggi multi-byte come il giapponese o il cinese.
  <item>Correntemente � disponibile il supporto per le pagine di manuale
    nelle seguenti lingue: tedesco, spagnolo, finlandese, francese,
    ungherese, italiano, giapponese, coreano e polacco attraverso i
    pacchetti <tt>manpages-LANG</tt> (dove LANG � il codice ISO a due lettere dello stato). Per
    accedere alla pagina di un manuale NLS, l'utente deve impostare la
    variabile LC_MESSAGES della shell alla stringa appropriata.
    <p>Per esempio, nel caso delle pagine di manuale in lingua italiana,
    LC_MESSAGES deve essere impostato a 'italian'. Il programma <prgn/man/ cercher�
    poi le pagine del manuale in italiano sotto la directory <tt>/usr/share/man/it/</tt>.
</list>

<sect id="usexports">A proposito delle regole statunitensi sulla limitazione dell'esportazione?

<p>Le leggi statunitensi pongono delle restrizioni sull'esportazione di articoli per la difesa,
inclusi alcuni tipi di software crittografici. PGP e ssh, assieme ad altri,
appartengono a questa categoria.

<p>Per prevenire chiunque da rischi legali non necessari, alcuni pacchetti
Debian GNU/Linux sono disponibili solo da un sito non-US
<url id="ftp://non-US.debian.org/debian-non-US/">. Ci sono numerosi
siti mirror anche fuori dagli Stati Uniti, si veda
<url id="ftp://non-US.debian.org/debian-non-US/README.non-US"> per una lista
completa.

<sect id="pine">Dov'� pine?

<p>A causa della sua licenza restrittiva, � nell'area non-free. Inoltre,
poich� la licenza non permette nemmeno ai binari modificati di essere distribuiti,
si deve compilarlo da soli dai sorgenti e dalle patch Debian.

<p>Il nome del pacchetto dei sorgenti � <package/pine/. Si pu� usare il
pacchetto <package/pine-tracker/ per essere avvisati su quando sar� necessario
aggiornare.

<p>Si noti che ci sono molti sostituti sia per pine che per pico, come
<package/mutt/ e <package/nano/, che si trovano nella sezione main.