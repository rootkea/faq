<!-- Subversion revision of original English document "3548" -->

<chapt id="compat">Probl�mes de compatibilit�

<sect id="arches">Sur quelle architecture mat�rielle fonctionne &debian;&nbsp;?

<p>&debian; fournit le code source complet pour tous les programmes, donc
elle peut fonctionner sur tous les syst�mes supportant le noyau Linux.
voir la <url name="FAQ Linux" id="http://en.tldp.org/FAQ/Linux-FAQ/intro.html#DOES-LINUX-RUN-ON-MY-COMPUTER">
pour plus de d�tails.

<!-- XXX update for new distros -->
<p>La version actuelle de &debian;, la &release;, contient une distribution 
binaire compl�te pour les architectures suivantes&nbsp;:

<p><em/i386/&nbsp;: ceci couvre les ordinateurs PCs bas�s sur les processeurs Intel et compatible,
incluant les processeurs Intel 386, 486, Pentium, Pentium Pro, Pentium II (Klamath
et Celeron), et Pentium III, et la plupart des processeurs compatibles de AMD, Cyrix
et autres.

<p><em/m68k/&nbsp;: ceci couvre les machines Amigas et ATARIs ayant un processeur Motorola 680x0
pour x>=2 avec MMU.

<p><em/alpha/&nbsp;: les syst�mes Alpha de Compaq/Digital.

<p><em/sparc/&nbsp;: ceci couvre les SPARC de SUN et la plupart des syst�mes UltraSPARC.

<p><em/powerpc/&nbsp;: ceci couvre certaines machines IBM/Motorola PowerPC, incluant les machines
CHRP, PowerMac et PReP.

<p><em/arm/&nbsp;: les machines ARM et StrongARM.

<p><em/mips/&nbsp;: les syst�mes MIPS gros-boutiens de SGI, Indy et Indigo2;
<em/mipsel/&nbsp;: les machines MIPS petit-boutiennes, Digital DECstations.

<p><em/hppa/&nbsp;: les machines PA-RISC de Hewlett-Packard (712, C3000, L2000, A500).

<p><em/ia64/&nbsp;: les ordinateurs Intel IA-64 (�&nbsp;Itanium&nbsp;�).

<p><em/s390/&nbsp;: les syst�mes mainframe S/390 de IBM.

<p>Le d�veloppement d'une distribution binaire Debian pour les architectures Sparc&nbsp;64
(native UltraSPARC) est actuellement en cours.

<p>Pour de plus amples informations sur le d�marrage, le partitionnement du disque, 
l'activation des p�riph�riques PCMCIA (carte PC) et sur des questions semblables 
veuillez-vous r�f�rer aux instructions du manuel d'installation, qui est disponible sur notre 
site web � l'adresse <url id="http://www.debian.org/releases/stable/installmanual">.

<sect id="otherdistribs">Debian est-elle compatible avec les autres distributions&nbsp;?	

<p>Les d�veloppeurs Debian communiquent avec les responsables des autres distributions Linux 
pour maintenir la compatibilit� binaire entre les distributions Linux.
La plupart des produits Linux commerciaux fonctionnent aussi bien sous Debian que sur le syst�me
pour lequel ils ont �t� construits.

<p>&debian; se conforme � la <url name="norme de hi�rarchie du syst�me de fichiers Linux" 
id="http://www.pathname.com/fhs/"> (�&nbsp;Linux Filesystem Hierarchy Standard&nbsp;�).
Cependant, certaines des r�gles de cette norme laissent une part � l'interpr�tation, de ce fait 
il peut y avoir de l�g�res diff�rences entre un syst�me Debian et les autres syst�mes Linux.

<sect id="otherunices">Comment le code source compatible Debian l'est-il avec d'autres 
			syst�mes Unix&nbsp;?
	
<p>Pour la plupart des applications, le code source de Linux est compatible avec les autres syst�mes Unix.
Il g�re presque tout qui est disponible pour les syst�mes Unix de type syst�me V et
pour les syst�mes d�riv�s des BSD libres et commerciaux. Cependant dans le monde Unix une 
telle d�claration n'a presque aucune valeur parce qu'il n'y a aucune possibilit� de la prouver.
Dans le secteur du d�veloppement logiciel la compatibilit� compl�te est exig�e et pas seulement une  
compatibilit� dans �&nbsp;la plupart des&nbsp;� cas. Ainsi il y a des ann�es le besoin de normes
s'est ressenti, et de nos jours POSIX.1 (norme 1003.1-1990 d'IEEE) est une des normes principales 
pour la compatibilit� du code source des syst�mes d'exploitation de type Unix.

<p>Linux est pr�vu pour adh�rer � POSIX.1, mais les normes de POSIX ne sont pas
gratuites et la certification POSIX.1 (et FIPS 151-2) est tr�s ch�re&nbsp;;
ce qui fait qu'il est tr�s difficile pour les d�veloppeurs Linux de travailler sur 
une conformit� compl�te de la norme Posix. Les co�ts de certification rendent peu probable 
l'obtention par Debian d'une certification officielle de conformit� m�me s'il passait
compl�tement la suite de validation (la suite de validation est maintenant disponible 
gratuitement, ainsi on s'attend � ce que plus de personnes travaillent sur les questions POSIX.1).

<!-- <p><strong>(The authors would very much like to give you a pointer to
an on-line document that described that standard, but the IEEE is another
one of those organizations that gets away with declaring standards and then
requiring that people PAY to find out what they are.  This makes about as
much sense as having to find out the significance of various colored lights
on traffic signals.)</strong> -->

<p>Unifix GmbH (Braunschweig, Allemagne) a d�velopp� un syst�me Linux qui a
�t� certifi� conforme � la norme FIPS 151-2 (un sur-ensemble de POSIX.1).
Cette technologie �tait disponible dans une distribution de Unifix appel�e
Unifix Linux 2.0 et dans Linux-FT de Lasermoon.

<!-- I had to comment this out for obvious reasons... SVD
 <url name="Linux-FT" url="http://www.lasermoon.co.uk/linux-ft/linux-ft.html">.
Currently Unifix merges its patches into the Linux kernel, gcc and other
tools; so it is expected that their fixes towards POSIX.1 conformance will
be available in Debian (and other distributions).
-->

<!-- TODO: explain better how to unpack .debs onto non-Debian systems -->
<sect id="otherpackages">Peut-on utiliser des paquets Debian (fichiers �&nbsp;.deb&nbsp;�) sur un syst�me
     Linux Red Hat/Slackware/...&nbsp;? Peux-t-on  utiliser des paquets Red Hat (fichiers �&nbsp;.rpm&nbsp;�) 
     sur un syst�me &debian;&nbsp;?

<p>Les diff�rentes distributions Linux utilisent des formats de paquets et des 
programmes de gestion de paquets diff�rents.

<taglist>
<tag><strong/Vous pouvez probablement&nbsp;:/
  <item>Un programme pour installer un paquet de Debian sur une machine Linux d'une autre distribution
  est disponible et fonctionnera g�n�ralement, dans le sens que les fichiers seront d�sarchiv�s.
  L'inverse est probablement vraie �galement, c.-�-d., un programme pour d�sarchiver un paquet Red Hat ou 
  Slackware sur une machine &debian; r�ussira probablement avec succ�s � d�sarchiver le paquet et � 
  placer la plupart des fichiers dans leur r�pertoire pr�vu. C'est en grande partie une cons�quence de
  l'existence (et de la large adh�rence) � la norme de hi�rarchie du syst�me de fichiers Linux.
  Le paquet <url name="alien" id="http://packages.debian.org/alien"> est utilis� 
  pour convertir les diff�rents formats des paquets.

<tag><strong/Vous ne voulez probablement pas&nbsp;:/
  <item>La plupart des responsables de paquet �crivent des fichiers de contr�le utilis�s 
  pour le d�sarchivage des paquets. Ces fichiers de contr�le ne sont g�n�ralement
  pas standardis�s. Par cons�quent, l'effet de d�sarchiver un paquet Debian sur une 
  machine d'une autre distribution aura (certainement) des effets impr�visibles sur 
  le gestionnaire de paquets du syst�me. De m�me, les utilitaires d'autres distributions 
  pourraient r�ussir � d�paqueter leurs archives sur des syst�mes Debian, mais feront 
  probablement �chouer le syst�me de gestion de paquets de Debian quand le temps viendra 
  d'am�liorer ou d'enlever quelques paquets, ou m�me simplement pour rapporter exactement 
  quels paquets sont pr�sents sur un syst�me.

<tag><strong/Une meilleure m�thode&nbsp;:/
  <item>La norme de syst�me de fichiers Linux (et donc aussi de Debian GNU/Linux) exige que le sous-r�pertoire
  <tt>/usr/local/</tt> soit enti�rement � la discr�tion de l'utilisateur. Donc les utilisateurs peuvent d�sarchiver
  leur paquet dans ce r�pertoire, puis administrer leur configuration, les mettre � niveau et les d�placer individuellement.
   </taglist>

<!-- It would be useful to document all the ways in which Debian and Red Hat
systems differ.  I believe such a document would do a lot to dispell
fears of using a Unix system. SGK -->

<!-- *******************Section supprimer dans la revision 1.14
<sect id="a.out">Debian est-il capable d'ex�cuter mes tr�s vieux programmes �&nbsp;a.out&nbsp;�&nbsp;?

<p>Actuellement, vous avez encore de tel programme&nbsp;? :-)


<p>Pour <em>ex�cuter</em> un programme dont le binaire est au format 
<tt>a.out</tt> (c.-�-d. QMAGIC ou ZMAGIC).

<list>
  <item>Assurez-vous que votre noyau a �t� construit avec la gestion du format <tt>a.out</tt>
  activ�, directement (CONFIG_BINFMT_AOUT=y) ou en module (CONFIG_BINFMT_AOUT=m).
  (les images Debian du noyau contiennent le module <tt>binfmt_aout</tt>).

  <p>Si votre noyau g�re les binaires <tt>a.out</tt> par module, v�rifiez que
  le module est bien charg�. Vous pouvez le charger au d�marrage en rajoutant la 
  ligne <tt>binfmt_aout</tt> dans le fichier <tt>/etc/modules</tt>. Ou depuis la 
  ligne de commande, en tapant <tt>insmod NOMREP/binfmt_aout.o</tt> o� <tt>NOMREP</tt>
  est le nom du r�pertoire o� les modules qui ont �t� construits pour la version du noyau
  fonctionnant maintenant sont stock�s. Pour un syst�me avec un noyau 2.2.17, <tt>NOMREP</tt> 
  doit ressembler � <tt>/lib/modules/2.2.17/fs/</tt> .
  
  <item>Installez le paquet <package/libc4/, recherchez le dans une version ant�rieur � la
  version 2.0 (parce que maintenant le paquer a �t� supprim�). Vous pouvez le rechercher 
  sur un vieux c�d�rom Debian (le paquet est pr�sent sur une Debian 1.3.1), ou sur Internet 
  allez voir <url id="ftp://archive.debian.org/debian-archive/">.
  
  <item>Si le programme que vous voulez lancer est un client X au format <tt>a.out</tt>
  alors vous devez installer le paquet <package/xcompat/ (voir au-dessus pour la disponibilit�).
</list>

<p>Si vous avez une application commerciale au format <tt>a.out</tt>, maintenant 
ce serait le bon moment de demander une mise � jour au format  <tt>ELF</tt>.
-->
<sect id="libc5">Debian est-elle capable d'ex�cuter de vieux programmes bas�s sur la libc5&nbsp;?

<p>Oui. Installez simplement la biblioth�que <package/libc5/ n�cessaire, depuis la section
<tt>oldlibs</tt> (contenant des paquets obsol�tes gard�s pour compatibilit� avec d'anciennes 
applications).


<sect id="libc5-compile">Debian peut-elle �tre utilis�e pour compiler des programmes avec la libc5&nbsp;?

<p>Oui. Installez les paquets <package/libc5-altdev/ et <package/altgcc/ (depuis
la section <tt>oldlibs</tt>). Vous trouverez les compilateurs <prgn/gcc/ et <prgn/g++/
compil�s avec la libc5 dans le r�pertoire <tt>/usr/i486-linuxlibc1/bin</tt>. Mettez 
ce chemin dans votre variable $PATH de fa�on � ce que make et les autres programmes 
les ex�cutent en premier.

<!-- *******************Supprimer dans la revision 1.15
<p>Si vous avez besoin de compiler des clients X avec la libc5, installez les paquets 
package/xlib6/ et <package/xlib6-altdev/.
-->

<p>Faite attention que l'environnement libc5 n'est plus compl�tement g�r� par nos autres 
paquets.

<sect id="non-debian-programs">Comment installer un programme n'appartenant pas � Debian&nbsp;?

<p>Les fichiers sous le r�pertoire <tt>/usr/local/</tt> ne sont pas sous le contr�le du
syst�me de gestion des paquets Debian. Ainsi, c'est une bonne habitude de mettre le code 
source de vos programmes dans le r�pertoire <tt>/usr/local/src</tt>. Par exemple, vous pouvez 
extraire les fichiers d'une archive appel�e �&nbsp;foo.tar&nbsp;� dans le r�pertoire 
<tt>/usr/local/src/foo</tt>. Apr�s la compilation, d�posez les binaires dans <tt>/usr/local/bin/</tt>, 
les biblioth�ques dans <tt>/usr/local/lib/</tt> et les fichiers de configuration dans
<tt>/usr/local/etc/</tt>.

<p>Si vos programmes et/ou fichiers doivent vraiment �tre plac�s dans un autre r�pertoire, 
vous pourriez les stocker tout de m�me dans <tt>/usr/local/</tt>, et �tablir les liens
symboliques appropri�s de l'endroit exig�  vers son emplacement dans <tt>/usr/local/</tt>, 
par exemple, vous pourriez faire le lien <example>ln -s /usr/local/bin/foo /usr/bin/foo</example>

<p>De toute fa�on, si vous r�cup�rez un programme dont le copyright permet la redistribution, 
vous devriez consid�rer la possibilit� d'en faire un paquet Debian et de le t�l�charger 
pour le syst�me de Debian. Les directives pour devenir un responsable de paquet sont incluses 
dans le manuel de la charte Debian (voir <ref id="debiandocs">).

<!-- *******************Section supprimer dans la revision 1.15
<sect id="xlib6">Pourquoi est-ce que j'obtiens le message d'erreur �&nbsp;Can't find libX11.so.6&nbsp;� 
	quand j'essaye d'ex�cuter <tt>foo</tt>&nbsp;?

<p>Ce message d'erreur signifie que le programme est li� avec une version <tt>libc5</tt>
des biblioth�ques X11. Dans ce cas vous avez besoin d'installer le paquet <package/xlib6/ 
depuis la section <tt>oldlibs</tt>.

<p>Vous pouvez recevoir un message semblable d'erreur se rapportant au fichier libXpm.so.4, dans ce cas
vous devez installer la version libc5 de la biblioth�que XPM, depuis le paquet <package/xpm4.7/ aussi 
disponible dans la section <tt>oldlibs</tt>.
-->

<sect id="termcap">Pourquoi ne peut-on pas compiler de programme n�cessitant 
		libtermcap&nbsp;?
	
<p>Debian emploie la base de donn�es de terminfo et la biblioth�que de ncurses pour les interfaces 
des terminaux, plut�t que la base de donn�es et la biblioth�que de termcap. Les utilisateurs qui
compilent des programmes demandant des connaissances sur l'interface terminal devraient remplacer 
les r�f�rences � <tt>libtermcap</tt> par des r�f�rences � <tt>libncurses</tt>.

<p>Pour faire fonctionner les binaires qui ont d�j� �t� li�s avec la biblioth�que <tt>termcap</tt> et dont 
vous n'avez pas les sources, Debian fournit un paquet appel� <package/termcap-compat/. Ceci fournit
les deux biblioth�ques <tt>libtermcap.so.2</tt> et <tt>/etc/termcap</tt>. Installez ce paquet si le 
programme refuse de s'ex�cuter et affiche le message d'erreur �&nbsp;can't load library 'libtermcap.so.2'&nbsp;�,
ou ce plaint au sujet du fichier <tt>/etc/termcap</tt> manquant.

<sect id="accelx">Pourquoi ne peut-on pas installer AccelX&nbsp;?
	
<p>AccelX utilise la biblioth�que <tt>termcap</tt> pour son installation. Voir
<ref id="termcap"> ci-dessus.

<sect id="motifnls">Pourquoi mes vieilles applications Motif sous XFree 2.1 plantent-elles&nbsp;?

<p>Vous avez besoin d'installer le paquet <package/motifnls/ qui fournit les fichiers de configuration
de XFree-2.1 n�cessaire pour permettre aux applications compil�es sous XFree-2.1 de tourner sous
XFree-3.1.


<p>Sans ces fichiers, certaine applications Motif compil�es sur une autre machine
(comme Netscape) peuvent se planter lors d'un copier-coller depuis ou vers un champs texte
et il peut y avoir d'autres probl�mes.
