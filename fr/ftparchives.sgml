<!-- Subversion revision of original English document "3548" -->

<chapt id="ftparchives">Les archives FTP de Debian

<sect id="dirtree">Quels sont tous ces r�pertoires dans les archives FTP de Debian&nbsp;?

<p>Le logiciel empaquet� pour &debian; est disponible dans un des nombreux r�pertoires
pr�sent sur chaque site miroir de Debian.

<p>Le r�pertoire <tt>dists</tt> est une abr�viation pour �&nbsp;distributions&nbsp;�
et c'est la mani�re canonique pour acc�der � la version (et pr�-versions) Debian
actuellement disponible.

<p>Le r�pertoire <tt>pool</tt> contient les paquets actuels, voir
<ref id="pools">.

<p>On trouve aussi les r�pertoires suppl�mentaires suivant&nbsp;:
<taglist>
  <tag><em>/tools/</em>&nbsp;:
    <item>Utilitaire DOS pour la cr�ation de disque de d�marrage, pour le partitionnement
	de votre disque, pour la compression et la d�compression de fichiers et pour le
	d�marrage de Linux.
  <tag><em>/doc/</em>&nbsp;:
    <item>La documentation de base de Debian, comme la FAQ, les instructions pour le syst�me de
		  rapport de bogues, etc.
  <tag><em>/indices/</em>&nbsp;:
    <item>Le fichier Maintainers et les fichiers override.
  <tag><em>/project/</em>&nbsp;:
    <item>Principalement des ressources pour les d�veloppeurs, comme&nbsp;:
    <taglist>
      <tag><em>project/experimental/</em>&nbsp;:
        <item>Ce r�pertoire contient des paquets et des outils qui sont encore en 
		d�veloppement et sont encore en �tat de test alpha. Les utilisateurs
		ne devraient pas utiliser ces paquets parce qu'ils peuvent �tre dangereux 
		m�me pour les personnes les plus exp�riment�es.
	
    </taglist>
</taglist>

<sect id="dists">Combien de distributions de Debian y a-t-il dans le r�pertoire 
			<tt>dists</tt>&nbsp;?

<p>Il y a trois distributions, la distribution �&nbsp;stable&nbsp;�, la distribution 
�&nbsp;testing&nbsp;�, et la distribution �&nbsp;unstable&nbsp;�. La distribution 
�&nbsp;testing&nbsp;� est quelque fois gel�e (�&nbsp;frozen&nbsp;�) (voyez <ref id="frozen">).

<sect id="codenames">� quoi correspondent tous les noms comme slink, potato, etc.&nbsp;?

<p>Ce sont justes des noms de code. Quand une distribution Debian est en cours de d�veloppement, 
elle n'a aucun num�ro de version mais un nom de code. Le but de ces noms de code est de faciliter
la copie sur les miroirs des distributions Debian (si un v�ritable r�pertoire comme <tt>unstable</tt> est 
soudainement renomm� en <tt>stable</tt>, beaucoup de choses devraient �tre inutilement t�l�charg�es).

<p>Actuellement, la version <tt>stable</tt> est un lien symbolique vers  <tt>&releasename;</tt>
(c.-�-d. Debian GNU/Linux 3.1) et la version  <tt>testing</tt> est un lien symbolique vers
<tt>&testingreleasename;</tt>.  Ceci signifie que <tt>&releasename;</tt> est la distribution 
�&nbsp;stable&nbsp;� actuelle et <tt>&testingreleasename;</tt> est la distribution �&nbsp;testing&nbsp;� actuelle.

<p><tt>Unstable</tt>  est un lien symbolique permanent vers <tt>sid</tt>, car <tt>sid</tt> 
est toujours la distribution �&nbsp;unstable&nbsp;�.

<sect1 id="oldcodenames">Quels noms de code ont d�j� �t� utilis�s&nbsp;?

<p>Les autres noms de code qui ont d�j� �t� employ�s sont&nbsp;: <tt>buzz</tt> pour la version 1.1, 
<tt>rex</tt> pour la version 1.2, <tt>bo</tt> pour les versions 1.3.x, <tt>hamm</tt> pour la version 2.0,
<tt>slink</tt> pour la version 2.1, <tt>potato</tt> pour la version 2.2 et <tt>woody</tt> pour la version 3.0.

<sect1 id="sourceforcodenames">D'o� proviennent les noms de code&nbsp?
 
<p>Jusqu'ici les noms de code proviennent des personnages du film  �&nbsp;Toy Story&nbsp;� par Pixar.
<list>
  <item><em>buzz</em> (Buzz Lightyear) est le cosmonaute,
  <item><em>rex</em> est le tyrannosaure,
  <item><em>bo</em> (Bo Peep) est la berg�re,
  <item><em>hamm</em> est la tirelire en forme de cochon,
  <item><em>slink</em> (Slinky Dog �) est le chien,
  <item><em>potato</em> est bien s�r, Mr. Patate �,
  <item><em>woody</em> est le cowboy,
  <item><em>sarge</em> est le sergent de l'arm�e de plastique vert,
  <item><em>etch</em>  est l'ardoise magique (Etch-a-Sketch �),

<!-- SID should be the last line always -->
  <item><em>sid</em> est le gar�on des voisins qui d�truit les jouets.
</list>

<!-- Q: Should we add the trademark info here? Maybe as a footnote
  Mr. Potato is a Registered Trademark of Playskool, Inc.,
    Pawtucket, R.I., a division of Hasbro Inc.
  Slinky Dog is a trademark of Poof Products of Plymouth, Mich.,
  Etch-a-Sketch is a trademark of The Ohio Art Company,
  other characters might also be registered trademarks...
  (jfs)
-->
<!--
  more info in http://www.pixar.com/featurefilms/ts/
  and  http://www.pixar.com/featurefilms/ts2/
  or better yet http://us.imdb.com/M/title-exact?Toy%20Story%20(1995)
  or actually:
    http://us.imdb.com/Title?0114709 for TS1
    http://us.imdb.com/Title?0120363 for TS2
  we shouldn't put the links in, Pixar needs no additional propaganda
-->
<!--
  characters not used from Toy Story (yet):
    - Andy (the kid)
    - Snake
    - Robot
    - Scud (Sid's dog)
    - Lenny the Binoculars
    - Three Eyed Alien
    - Rocky (the wrestling figure)
    - Roller Bob (the remote control car)
    - Legs (one of sid's mutant toys)
    - Hand-in-the-box (one of sid's mutant toys)
    - Duckie (one of sid's mutant toys)
  and additional characters from Toy Story 2, also not yet used:
    - Al (the propietor of Al's Toy Farm)
    - Jessie (the Yodelling Cowgirl)
    - Bullseye (Woody's toy horse)
    - Zurg (the Evil Emperor)
    - Wheezy (the penguin)
    - Hannah (owner of Jessie)
    - Stinky Pete the Prospector (the old fat guy)
    - Mrs. Davis (Andy's Mom)
    - Barbie (the Tour Guide, probably under (c))
    - Mrs. Potato Head
    - Heimlich the Caterpillar
-->
<!-- (jfs) Just in case somebody misses the "What do we do when we finish
with Toy Story characters" thread see:
http://lists.debian.org/debian-devel/2002/debian-devel-200207/msg01133.html
I, suggested we followed with either Monster's Inc or "A Bug's life" :)
-->

<sect id="sid">Que dire de �&nbsp;sid&nbsp;�&nbsp;?

<p><em>Sid</em> ou <em>�&nbsp;unstable&nbsp;�</em> est le lieu o� la plupart des paquets sont
initialement t�l�charg�s. Elle ne sera jamais directement publi�e, parce que les 
paquets devront d'abord �tre inclus dans <em>�&nbsp;testing&nbsp;�</em>, afin d'�tre publi�s 
dans <em>�&nbsp;stable&nbsp;�</em> plus tard. Sid contient des paquets pour l'ensemble des 
architectures publi�es ou non.

<p>Le nom �&nbsp;sid&nbsp;� vient aussi du film d'animation Toy Story&nbsp;:
Sid est le gar�on des voisins qui d�truit les jouets.

<p><footnote>
<p>Quand la sid n'existait pas, l'organisation du site FTP avait un d�faut majeur. 
On supposait que quand une architecture �tait cr��e dans la distribution 
�&nbsp;unstable&nbsp;� actuel, elle serait publi�e quand la distribution
deviendrait la nouvelle distribution �&nbsp;stable&nbsp;�. Pour beaucoup d'architectures 
ce n'�tait pas le cas, ce qui obligeait � d�placer ces r�pertoires lors de la sortie d'une 
version. Ce n'�tait pas pratique parce que le d�placement aurait consomm� beaucoup de bande 
passante.

<p>Les administrateurs ont �vit� le probl�me pendant plusieurs ann�es en pla�ant 
les binaires pour les architectures non publi�es dans un r�pertoire particulier
nomm� �&nbsp;sid&nbsp;�. Pour les architectures non encore publi�es, lors de leurs 
sorties, un lien entre la stable courante et sid �tait cr�� et � partir de l� elles 
�taient cr��es normalement dans l'arborescence �&nbsp;unstable&nbsp;�. Cette disposition 
�tait l�g�rement d�concertante pour les utilisateurs.

<p>Avec l'arriv�e des r�pertoires communs (voir <ref id="pools">), les paquets
ont commenc� � �tre plac�s dans un endroit standard dans le pool, quelle que soit 
la distribution, donc la publication d'une distribution n'entra�ne plus de consommation
excessive de bande passante sur les miroirs (mais il y a cependant une consommation
graduelle de la bande passante pendant le processus de d�veloppement).
</footnote>

<sect id="stable">Que contient le r�pertoire stable&nbsp;?

<p><list>
  <item>stable/main/&nbsp;:
   Ce r�pertoire contient les paquets qui constituent la version la plus r�cente 
  du syst�me &debian;.
  
  <p>Ces paquets sont tous conformes aux <url name="principes du logiciel libre selon Debian"
  id="http://www.debian.org/social_contract#guidelines"> (Debian Free Software Guidelines,
  DFSG) et sont tous librements utilisables et librements distribuables.

  <item>stable/non-free/&nbsp;:  Ce r�pertoire contient les paquets de la distribution ayant certaines 
  restrictions, ce qui oblige que les distributeurs doivent tenir compte soigneusement des conditions 
  d�finies dans les copyright.

  <p>Par exemple, certains paquets ont une licence avec une clause interdisant 
  une distribution commerciale. D'autres peuvent �tre redistribu�s mais sont en fait
  des partagiciels (shareware) et non des graticiels (freeware). La licence de chaque paquet doit �tre �tudi�e,
  et probablement n�goci�e, avant qu'ils ne soient inclus dans toutes distributions
  (par exemple sur un c�d�rom). 

  <item>stable/contrib/&nbsp;: Ce r�pertoire contient les paquets qui sont conformes aux DFSG et 
  <em>librement distribuables</em>, mais d�pendent d'une fa�on ou d'une d'autre d'un paquet qui
  <em/n'est pas/ librement distributable et ainsi disponible seulement dans la section non-free.
</list>

<sect id="testing">Que contient le r�pertoire testing&nbsp;?

<p>Des paquets sont install�s dans le r�pertoire �&nbsp;testing&nbsp;� apr�s qu'ils aient subi 
un certain nombre de tests dans �&nbsp;<qref id="unstable">unstable</qref>&nbsp;�.

<p>Ils doivent �tre synchronis�s sur toutes les architectures o� ils ont �t� construits 
et ne doivent pas avoir de d�pendances qui emp�cheraient leur installation&nbsp;; ils doivent 
�galement avoir moins de bogues critiques que les versions actuellement dans �&nbsp;testing&nbsp;�. 
De cette fa�on, nous esp�rons que �&nbsp;testing&nbsp;� soit une version toujours pr�te � la publication.

<p>Plus d'informations sur l'�tat de �&nbsp;testing&nbsp;� en g�n�ral et sur les diff�rents paquets 
sont disponibles � <url id="http://www.debian.org/devel/testing">.

<sect1 id="frozen">Que dire de �&nbsp;testing&nbsp;�&nbsp;? Comment est-elle
			gel�e (�&nbsp;frozen&nbsp;�)&nbsp;?

<p>Quand la distribution �&nbsp;testing&nbsp;� est suffisamment mature, le gestionnaire
de version commence � geler la distribution. Le temps de propagation des paquets entre les 
distribution est augment� pour s'assurer que le moins possible de bogues passe de la distribution 
�&nbsp;unstable&nbsp;� dans �&nbsp;testing&nbsp;�.

<p>Apr�s un moment, la distribution �&nbsp;testing&nbsp;� devient vraiment gel�e.
Ceci signifie que tous les nouveaux paquets qui devait entrer dans �&nbsp;testing&nbsp;�
sont bloqu�s, � moins qu'il ne corrige un bogue critique (release critical).
La distribution �&nbsp;testing&nbsp;� peut �galement demeurer dans un gel profond pendant 
les cycles d'essai, quand la publication est imminente.

<p>Nous conservons un enregistrement des bogues de la distribution �&nbsp;testing&nbsp;�
qui peuvent emp�cher un paquet d'�tre publi�, ou retarder la publication de la
distribution. Pour plus de d�tails, veuillez-vous reporter aux <url name="informations
sur la version testing actuelle" id="http://www.debian.org/releases/testing/">.

<p>Une fois que le nombre de bogues a atteint une valeur maximale acceptable,
la distribution �&nbsp;testing&nbsp;� gel�e est d�clar�e �&nbsp;stable&nbsp;�
et publi�e avec un num�ro de version.

<p>Avec chaque nouvelle version, l'ancienne distribution �&nbsp;stable&nbsp;� devient
obsol�te et est d�plac�e de l'archive. pour plus d'informations, reportez-vous �
<url name="Debian archive" id="http://www.debian.org/distrib/archive">.

<sect id="unstable">Que contient le r�pertoire unstable&nbsp;?

<p>Le r�pertoire �&nbsp;unstable&nbsp;� contient une image du syst�me en cours de
d�veloppement. Les utilisateurs sont les bienvenus pour utiliser et tester ces
paquets, mais soyez averti au sujet de leur �tat. L'avantage d'utiliser la 
distribution �&nbsp;unstable&nbsp;� est que votre syst�me est toujours � jour
avec la derni�re version des logiciels GNU/Linux, mais s'il y a un probl�me, 
vous en d�couvrirez les mauvais c�t�s.

<p>Il y a aussi dans �&nbsp;unstable&nbsp;� des sous-r�pertoires main, contrib et 
non-free remplis avec les m�mes crit�res que dans �&nbsp;stable&nbsp;�.

<sect id="archsections">Que sont tous ces r�pertoires dans
  <tt>dists/stable/main</tt>&nbsp;?

<p>Dans chacune des arborescences des r�pertoires majeurs
<footnote>
  <tt>dists/stable/main</tt>, <tt>dists/stable/contrib</tt>,
  <tt>dists/stable/non-free</tt> et <tt>dists/unstable/main/</tt>, etc.
</footnote>, il y a trois ensembles de sous-r�pertoires contenant les fichiers 
catalogues.

<p>Il y a un ensemble de sous-r�pertoires <tt>binary-<var>quelquechose</var></tt>
contenant les fichiers catalogues pour les paquets binaires de chaque architecture 
disponible. Par exemple, <tt/binary-i386/ pour les paquets s'ex�cutant sur les machines 
Intel x86 ou <tt/binary-sparc/ pour les paquets s'ex�cutant sur les SPARCStations Sun.

<p>La liste compl�te de toutes les architectures disponibles pour chaque version est 
accessible � l'adresse <url name="the release's web page" id="http://www.debian.org/releases/">.
Pour la version en cours, veuillez-vous reporter � <ref id="arches">.

<p>Les fichiers catalogues dans les r�pertoires <tt>binary-*</tt> sont nomm�s
Packages(.gz) et fournissent un r�sum� de chaque paquets binaires pr�sents dans
la distribution. Les paquets binaires  (pour <em/woody/ et les versions suivantes)
se trouvent � la racine du  <qref id="pools">r�pertoire <tt/pool/</qref>.

<p>De plus, il y existe un sous-r�pertoire nomm� <tt>source/</tt> contenant les fichiers 
catalogues pour les paquets sources contenus dans la distribution.
Le fichier catalogue est nomm� Sources(.gz).

<p>Le dernier mais non des moindre, est un ensemble de sous-r�pertoires utilis� pour les
fichiers catalogues du syst�me d'installation. Pour la version <em>woody</em> ils sont
nomm�s <tt>disks-<var>architecture</var></tt> et pour <em>sarge</em> c'est 
<tt>debian-installer/binary-<var>architecture</var></tt>.

<sect id="source">O� trouve-t-on le code source&nbsp;?

<p>Le code source est inclus pour tout le syst�me Debian. De plus, les termes
de la licence de la plupart des programmes du syst�me <em>requi�rent</em> que
le code source soit distribu� avec le programme, ou qu'un moyen de r�cup�rer
ce code accompagne le programme.

<p>Le code source est distribu� dans le r�pertoire <tt>pool</tt> (voir <ref id="pools">)
avec les binaires de toutes les architectures. Pour r�cup�rer le code source sans �tre 
familier avec la structure de l'archive FTP, ex�cutez une commande 
comme <tt>apt-get source monpaquet</tt>.

<p>Certains paquets sont seulement distribu�s sous forme de code source � cause
de restriction de leur licence. Notamment, l'un de ces paquets est <tt>pine</tt>,
voir <ref id="pine"> pour plus d'informations.

<p>Le code source peut ou non �tre disponible pour les paquets dans les sections
�&nbsp;contrib&nbsp;� et �&nbsp;non-free&nbsp;�, qui ne font pas formellement 
partie du syst�me Debian.

<sect id="pools">Que trouve-t-on dans le r�pertoire <tt>pool</tt>&nbsp;?

<p>Les paquets sont gard�s dans un r�pertoire commun (�&nbsp;pool&nbsp;�), structur� selon 
le nom des paquets sources. Pour rendre cela g�rable, le r�pertoire est divis� par section 
(�&nbsp;main&nbsp;�, �&nbsp;contrib&nbsp;� et �&nbsp;non-free&nbsp;�) et dans chaque section 
par la premi�re lettre du nom des paquets sources. Ces r�pertoires contiennent plusieurs 
fichiers&nbsp;: les paquets binaires pour chaque architecture et les paquets sources � partir 
desquels sont construits les paquets binaires.

<p>Vous pouvez voir o� chaque paquet est conserv� en ex�cutant la commande
<tt>apt-cache showsrc monpaquet</tt> et en regardant la ligne �&nbsp;Directory:&nbsp;�.
Par exemple, les paquets <tt>apache</tt> sont conserv�s dans <tt>pool/main/a/apache/</tt>.

<p>De plus, comme il existe de nombreux paquets de biblioth�que <tt>lib*</tt>, ceux-ci 
sont trait�s diff�remment. Par exemple, le paquet <tt>libpaper</tt> est plac� dans le 
r�pertoire <tt>pool/main/libp/libpaper/</tt>.

<!-- joeyh doesn't want to maintain it so it's dead; need to integrate it
     If you want more information, see the
     <url id="http://people.debian.org/~joeyh/poolfaq"
     name="Debian Package Pools FAQ">.
-->

<p><footnote>
<p>Historiquement, les paquets �taient conserv�s dans le sous-r�pertoire <tt>dists</tt>
correspondant aux distributions. Ceci s'est av�r� poser certains probl�mes, 
comme une grande consommation de bande passante sur des miroirs lors de changements 
majeurs. Ces probl�mes ont �t� r�solus avec l'introduction du r�pertoire pool.

<p>Le r�pertoire <tt>dists</tt> est encore utilis� pour les fichiers catalogues
servant � des programmes comme <tt>apt</tt>. Vous pouvez �galement voir les chemins 
<tt>dists/potato</tt> ou <tt>dists/woody</tt> dans les champs des ent�tes de fichier 
de certains anciens paquets.
</footnote>

<sect id="incoming">Qu'est-ce que le r�pertoire incoming&nbsp;?

<p>Apr�s qu'un d�veloppeur a envoy� un paquet, il est conserv� dans le
r�pertoire �&nbsp;incoming&nbsp;� avant de v�rifier son origine et de l'autoriser 
dans l'archive.

<p>G�n�ralement personne ne devrait installer des paquets provenant de ce 
r�pertoire. Cependant, dans certains rares cas d'urgence le r�pertoire �&nbsp;incoming&nbsp;�
est disponible � <url id="http://incoming.debian.org/">. Vous devrez 
t�l�charger manuellement les paquets, v�rifier les signatures GPG et les
sommes MD5 dans les fichiers .changes et .dsc et apr�s les installer.


