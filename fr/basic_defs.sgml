<!-- Subversion revision of original English document "3548" -->
<chapt id="basic_defs">D�finitions et vue d'ensemble

<sect id="whatisdebian">Qu'est-ce que &debian;&nbsp;? 

<p>&debian; est une distribution particuli�re du syst�me d'exploitation Linux
et des nombreux paquets qui fonctionnent dessus.

<p>En principe, les utilisateurs peuvent obtenir le noyau Linux via Internet ou
ailleurs et le compiler eux-m�mes. De la m�me mani�re, ils peuvent r�cup�rer le 
code source de nombreuses applications, compiler les programmes et les installer 
sur leur syst�me. Pour des programmes complexes, ce processus peut �tre non seulement 
long mais aussi source d'erreurs. Pour �viter cela , les utilisateurs choisissent 
souvent d'obtenir le syst�me d'exploitation et l'ensemble des applications d'un des
distributeurs de Linux. Ce qui distingue les diff�rentes distributions Linux, ce sont
les logiciels, les protocoles, et les pratiques employ�es pour empaqueter, installer
et v�rifier l'ensemble des applications sur les syst�mes des utilisateurs, combin�s 
avec des outils d'installation et d'administration, de la documentation, et d'autres 
services.

<p>&debian; est le r�sultat d'un effort de volontaires pour cr�er un syst�me d'exploitation, 
compatible Unix, libre et de bonne qualit�, compl�t� avec une suite d'applications.
L'id�e d'un syst�me libre de type Unix provient du projet GNU et plusieurs des applications 
qui rendent &debian; si utile ont �t� d�velopp�es par le projet de GNU.

<p>
Pour Debian, le terme libre a le sens donn� par le projet GNU (voir les <url name="principes 
du logiciel libre selon Debian" id="http://www.debian.org/social_contract#guidelines">).
Quand nous parlons de logiciel libre, nous faisons r�f�rence � la libert� et non au prix
(NdT&nbsp;: ambigu�t� du mot anglais �&nbsp;free&nbsp;� signifiant libre et gratuit). Logiciel libre
signifie que vous avez la libert� de distribuer des copies des logiciels, que vous recevez le code
source ou que vous pouvez l'obtenir si vous le voulez, que vous pouvez modifier le logiciel ou employer
des parties du code dans de nouveaux projets libres et que vous savez que vous pouvez faire tout
cela.

<p>Le projet Debian a �t� cr�� par Ian Murdock en 1993, initialement sous le
patronage du projet GNU de la  Free Software Foundation. Aujourd'hui, les 
d�veloppeurs Debian le voient comme un descendant direct du projet GNU.

<p>&debian; est&nbsp;:
<list>
<item><strong>compl�te</strong>&nbsp;: actuellement, Debian inclut plus de
&all-pkgs; logiciels. Les utilisateurs peuvent choisir quels paquets installer&nbsp;;
Debian fournit un outil � cette fin. Vous pouvez trouver une liste et la description
des paquets actuellement disponibles dans Debian sur n'importe quel
<url id="http://www.debian.org/distrib/ftplist" name="miroir"> Debian.

<item><strong>libre d'utilisation et de distribution</strong>&nbsp;: il n'y a aucune 
adh�sion ou paiement � un �tablissement exig� pour participer � sa distribution et � 
son d�veloppement. Tous les paquets qui font formellement parties de &debian; sont libres
d'�tre redistribu�s, g�n�ralement sous les termes de la licence GNU GPL.

<p>Les archives FTP de Debian fournissent �galement environ &contrib-nonfree-pkgs;
logiciels (dans les sections <tt>non-free</tt> et <tt>contrib</tt>), qui sont distribuables 
selon les conditions sp�cifiques incluses avec chaque paquet.

<item><strong>dynamique</strong>&nbsp;: avec environ &developers; volontaires 
qui contribuent constamment � la cr�ation et � l'am�lioration du code, Debian
�volue rapidement. De nouvelles versions sont pr�vues pour �tre construites chaque
mois et les archives FTP sont mises � jour quotidiennement.
</list>

<p>Comme &debian; est elle-m�me un logiciel libre, elle peut servir de base pour des distributions
Linux � valeur ajout�e. En fournissant un syst�me de base fiable et complet, Debian fournit aux 
utilisateurs Linux une compatibilit� accrue, et permet � des cr�ateurs de distribution Linux d'�liminer 
la duplication des efforts en se concentrant sur les choses qui rendent leur distribution particuli�re.
Voir <ref id="childistro"> pour plus d'informations.

<sect id="linux">D'accord, maintenant je sais ce qu'est Debian, qu'est-ce que Linux&nbsp;?

<p>En r�sum�, Linux est le noyau d'un syst�me d'exploitation de type Unix. Il
a �t� � l'origine con�u pour les ordinateurs 386 (et plus r�cent)&nbsp;; maintenant, 
le portage vers d'autres syst�mes, y compris des syst�mes multiprocesseurs, est 
en cours de d�veloppement. Linux a �t� d�velopp� par Linus Torvalds et beaucoup 
d'informaticiens du monde entier.

<p>En plus du noyau, un syst�me Linux contient habituellement&nbsp;:
<list>
  <item>un syst�me de fichiers qui suit la norme de hi�rarchie du syst�me de fichiers Linux 
  (�&nbsp;Filesystem Hierarchy Standard&nbsp;�, FHS) <url id="http://www.pathname.com/fhs/">,
  <item>un ensemble d'utilitaires Unix, dont la plupart ont �t� d�velopp�s par le projet GNU 
  et la Free Software Foundation.
</list>

<p>La combinaison du noyau Linux, du syst�me de fichiers, des utilitaires GNU de la FSF, 
et des autres utilitaires a �t� con�ue pour �tre en conformit� avec la norme POSIX 
(IEEE 1003,1). Voir <ref id="otherunices">.

<p>Pour plus d'informations sur Linux, voir les documents (en anglais) de Michael K. Johnson
<url id="ftp://ibiblio.org/pub/Linux/docs/HOWTO/INFO-SHEET" name="Linux Information Sheet">
et <url id="ftp://ibiblio.org/pub/Linux/docs/HOWTO/META-FAQ" name="Meta-FAQ">.


<sect id="hurd">Quelle est cette nouvelle chose, le �&nbsp;Hurd&nbsp;�&nbsp;?

<p>Le Hurd est un ensemble de serveurs s'ex�cutant au dessus du micro-noyau GNU Mach.
Ensemble, ils forment la base du syst�me d'exploitation GNU.

<p>Actuellement, Debian n'est disponible que pour Linux, mais avec Debian GNU/Hurd
nous avons aussi commenc� � fournir GNU/Hurd comme plate-forme de d�veloppement. 
Debian GNU/Hurd n'est pas encore officiellement publi�e, et ne sortira pas avant quelques temps.

<p>Veuillez vous reporter au site <url id="http://www.gnu.org/software/hurd/"> pour
plus d'informations en g�n�ral sur GNU/Hurd et au site <url id="http://www.debian.org/ports/hurd/">
pour des informations particuli�res � Debian GNU/Hurd.

<sect id="difference">Quelles sont les diff�rences entre &debian; et les autres distributions
  Linux&nbsp;? Pourquoi choisir Debian plut�t qu'une autre&nbsp;?

<p>Ces fonctions principales distinguent Debian des autres distributions Linux&nbsp;:
<taglist>
<tag/Le syst�me de gestion de paquets de Debian&nbsp;:/
  <item>Le syst�me entier ou n'importe quel composant individuel peut �tre mis
  � jour sans reformater, sans perdre les fichiers de configuration personnalis�s
  et (dans la plupart des cas) sans red�marrer le syst�me.  Aujourd'hui, la plupart 
  des distributions Linux disponibles ont leur propre syst�me de gestion de paquets&nbsp;;
  le syst�me de gestion de paquets de Debian est unique et particuli�rement robuste.
  (voir <ref id="pkg_basics">)
  <!-- This is unsatisfactory without some anecdotal or analytical
       evidence. SGK -->

<tag/D�veloppement ouvert&nbsp;:/
  <item>Alors que d'autres distributions Linux sont d�velopp�es par des individus, 
  des petits groupes ferm�s, ou des fournisseurs commerciaux. Debian est la seule 
  distribution Linux qui est d�velopp�e coop�rativement par beaucoup d'individus 
  gr�ce � Internet, dans le m�me esprit que Linux et d'autres logiciels libres.

  <p>Plus de &developers; responsables de paquet travaillent b�n�volement sur plus de &all-pkgs; 
  paquets et participent � l'am�lioration de &debian;. Les d�veloppeurs Debian contribuent au 
  projet non pas en programmant de nouvelles applications (dans la plupart des cas) mais en
  empaquetant les logiciels existants suivant les normes du projet, en envoyant des rapports 
  de bogue aux d�veloppeurs amont et en fournissant de l'aide aux utilisateurs. Voir aussi 
  les informations sur comment devenir un contributeur dans <ref id="contrib">.

<tag/Le syst�me de gestion des bogues&nbsp;:/
  <item>La r�partition g�ographique des d�veloppeurs Debian demande des outils sophistiqu�s 
  et une communication rapide des bogues et de leurs r�solutions pour acc�l�rer le d�veloppement 
  du syst�me. Les utilisateurs sont encourag�s � envoyer des rapports de bogue dans un mod�le 
  formel, qui est rapidement accessible par les archives web ou par courrier �lectronique. Reportez-vous aux 
  informations suppl�mentaires dans cette FAQ, sur la gestion des bogues dans <ref id="buglogs">.


  <!-- XXX develop a metric for bug-fixing
  We ought to have some metric that tells us exactly how fast bugs are
  fixed.  This would provide a challenge to the rest of the software industry.
  SGK

  "Until year 2000, our bug tracking system has processed over fifty thousand
  bug reports, one fifth of which are still open." -Joy  -->

<tag/La charte Debian&nbsp;:/
  <item>Debian a des sp�cifications �tendues des normes de qualit�, la charte Debian.
  Ce document d�finit les exigences de qualit� et les normes que doit satisfaire chaque paquet Debian. 
</taglist>

<p>Pour d'autres informations sur le sujet, veuillez voir la page web sur les
<url name="raisons pour choisir Debian" id="http://www.debian.org/intro/why_debian">.

<sect id="gnu">Comment le projet Debian s'ins�re-t-il avec le projet GNU de la Free
		Software Foundation&nbsp;?

<p>Le syst�me Debian est construit sur les id�aux du logiciel libre d'abord soutenus par la 
<url name="Free Software Foundation" id="http://www.gnu.org/"> et en particulier par
<url name="Richard Stallman" id="http://www.stallman.org/">. Les outils de d�veloppement puissants 
ainsi que les utilitaires, et les applications de la FSF sont �galement une partie principale du 
syst�me Debian.

<p>Le projet Debian est ind�pendant de la FSF, cependant nous communiquons r�guli�rement
et coop�rons sur diff�rents projets. La FSF a explicitement demand� que nous appelions
notre syst�me �&nbsp;&debian;&nbsp;� et nous sommes heureux de nous conformer � cette demande.

<p>L'objectif de longue date de la FSF est de d�velopper un nouveau syst�me d'exploitation appel� GNU, 
bas� sur <url name="Hurd" id="http://www.gnu.org/software/hurd/">.  Debian travaille avec la FSF sur 
ce syst�me, nomm� Debian <url name="Debian GNU/Hurd" id="http://www.debian.org/ports/hurd/">.


<sect id="pronunciation">Comment prononce-t-on Debian et quel est le sens de ce mot&nbsp;?
	
<p>Le nom du projet est prononc� Deb'-i-an, avec un e court dans Deb et l'accentuation
port�e sur la premi�re syllabe. Ce mot est la contraction des noms Debra et Ian Murdock,
qui a fond� le projet. (Les dictionnaires semblent laisser une certaine ambigu�t� dans
la prononciation de Ian, mais Ian pr�f�re i'-an.)


