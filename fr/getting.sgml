<!-- Subversion revision of original English document "3548" -->

<chapt id="getting">Obtenir et installer &debian;

<sect id="version">Quelle est la derni�re version de Debian&nbsp;?

<p>Actuellement il existe trois versions de &debian;&nbsp;:
<taglist>
  <tag><em/La version &release;, c.-�-d. la distribution �&nbsp;stable&nbsp;�/,
    <item>c'est la version stable et bien test�e, seuls sont incorpor�s les changements dus	
	� des probl�mes importants de s�curit� ou d'utilisation.
  <tag><em/La distribution �&nbsp;testing&nbsp;�/,
    <item>c'est dans cette distribution que sont plac�s les paquets qui seront dans la 
	prochaine distribution �&nbsp;stable&nbsp;�&nbsp;; ils ont d�j� �t� test�s lors de leur passage dans 
	�&nbsp;unstable&nbsp;� mais ne sont pas encore compl�tement pr�ts. Cette distribution est 
	mise � jour plus souvent que la distribution �&nbsp;stable&nbsp;�, mais moins souvent que 
	la distribution �&nbsp;unstable&nbsp;�.
  <tag><em/La distribution �&nbsp;unstable&nbsp;�/,
    <item>c'est la version actuellement en d�veloppement&nbsp;; elle est continuellement
	mise � jour. Vous pouvez r�cup�rer des paquets archives instables sur n'importe 
	quel site FTP de Debian et les employer pour am�liorer votre syst�me, mais vous 
	ne devez pas vous attendre � ce que le syst�me soit aussi utilisable ou aussi 
	stable qu'avant. C'est pourquoi cette version s'appelle �&nbsp;unstable&nbsp;�.
</taglist>

<p>Reportez-vous � la question <ref id="dists"> pour plus d'informations.

<sect id="updatestable">Existe-t-il des mises � jour de la distribution �&nbsp;stable&nbsp;�&nbsp;?

<p>Aucune nouvelle fonctionnalit� n'est ajout�e � la version stable.
Une fois qu'une version de Debian est publi�e et consid�r�e comme stable, 
les seules mises � jours de la distribution sont des mises � jour de s�curit�.
C'est-�-dire, que seuls les paquets pour lesquels une faille de s�curit� 
a �t� trouv�e apr�s la publication sont mis � jour.
Toutes les mises � jour de s�curit� sont effectu�es � partir du serveur
<url name="security.debian.org" id="ftp://security.debian.org">.</p>

<p>Les mises � jour de s�curit� ont un but&nbsp;: fournir une correction pour
une faille de s�curit�. Elles ne sont pas l� pour ajouter discr�tement
des modifications dans la version stable sans passer par la proc�dure normale de 
version interm�diaire. En cons�quence, les correctifs pour les paquets avec des 
probl�mes de s�curit� ne sont pas une mise � jour du logiciel, mais un r�troportage 
par l'�quipe de s�curit� du correctif n�cessaire pour la version distribu�e dans stable.
</p>

<p>Pour plus d'informations li�es � la gestion de la s�curit�, veuillez lire la
<url name="FAQ de l'�quipe Debian sur la s�curit�" id="http://www.debian.org/security/faq"> ou
le <url name="Manuel de s�curisation de Debian"
id="http://www.debian.org/doc/manuals/securing-debian-howto/">.


<sect id="boot-floppies">O� et comment puis-je obtenir les disques d'installation de Debian&nbsp;?

<p>Vous pouvez obtenir les disques d'installation en t�l�chargeant les fichiers appropri�s
depuis <url name="les miroirs Debian" id="http://www.debian.org/mirror/list">.

<p>Les fichiers du syst�me d'installation sont r�partis dans les sous-r�pertoires de
<file>dists/stable/main</file>, et les noms de ces sous-r�pertoires correspondent 
aux diff�rentes architectures comme ceci&nbsp;: <tt>disks-<var>arch</var></tt> 
(<var>arch</var> est �&nbsp;i386&nbsp;�, �&nbsp;sparc&nbsp;�, etc. Reportez-vous 
au site pour une liste exacte). Dans chacun de ces sous-r�pertoires d'architecture, il 
peut y avoir plusieurs r�pertoires, chacun pour une version du syst�me d'installation. 
Le syst�me actuellement utilis� est dans le r�pertoire �&nbsp;current&nbsp;� (c'est un lien symbolique).

<p>Lisez le fichier <file/README.txt/ dans ce r�pertoire pour des instructions
compl�mentaires.

<sect id="cdrom">Comment installer Debian depuis les c�d�roms&nbsp;?

<p>Linux g�re le syst�me de fichiers ISO&nbsp;9660 (CD-ROM) avec les extensions Rock Ridge
(autrefois connues sous le nom �&nbsp;High Sierra&nbsp;�"). Plusieurs <url name="vendeurs" 
id="http://www.debian.org/CD/vendors/"> fournissent &debian; sous ce format.

<p>Attention&nbsp;: quand vous faites une installation depuis les c�d�roms, c'est g�n�ralement 
une mauvaise id�e de choisir la m�thode d'acc�s au c�d�rom par dselect. Cette m�thode est
habituellement tr�s lente. Les m�thodes <tt/mountable/ et <tt/apt/, par exemple, sont
bien meilleures pour l'installation depuis un c�d�rom (voir <ref id="dpkg-mountable"> et <ref id="apt">).

<sect id="cdimages">J'ai mon propre graveur de c�d�rom, existe-t-il des images de c�d�rom
					disponibles quelque part&nbsp;?

<p>Oui. Pour permettre aux vendeurs de c�d�rom de fournir des disques de bonne qualit�,
nous fournissons des <url id="http://cdimage.debian.org/" name="images de c�d�rom officiel">.

<sect id="floppy">Puis-je faire l'installation depuis des disquettes&nbsp;?

<p>Avant de commencer, notez que l'ensemble de &debian; est trop important pour �tre 
install� depuis un m�dia aussi petit que les disquettes de 1,44&nbsp;Mo. Vous risquez de 
ne pas trouver tr�s agr�able une installation par ce type de m�dia.

<p>Copiez les paquets de Debian sur les disquettes format�es.
Vous pouvez utiliser les formats de syst�me de fichiers DOS, ext2 ou minix, 
vous devriez juste employer la commande mount appropri�e pour pouvoir 
utiliser vos disquettes.

<p>L'emploi de disquettes a comme inconv�nients&nbsp;:
<list>
  <item>Nom de fichier MS-DOS court&nbsp;: si vous essayez de mettre sur une disquette format�e avec MS-DOS 
	 un paquet Debian, vous constaterez que leurs noms sont g�n�ralement trop longs et ne sont pas 
	 conformes � la limitation des noms de fichier MS-DOS (8.3). Pour surmonter ceci, vous devriez 
	 utiliser le syst�me de fichiers VFAT, puisque VFAT permet la gestion des noms longs pour les fichiers.
   <item>Taille de fichier trop grande&nbsp;: certains paquets font plus de 1,44&nbsp;Mo et 
     ne peuvent pas �tre plac�s sur une simple disquette. Pour r�soudre ce probl�me, 
	 vous pouvez utiliser la commande dpkg-split (voir <ref id="dpkg-split">), disponible 
	 dans le r�pertoire <tt>tools</tt> sur <url name="les miroirs Debian" id="http://www.debian.org/mirror/list">.
</list>

<p>Votre noyau doit avoir la gestion des disquettes activ�e avant de pouvoir lire et 
�crire sur une disquette&nbsp;; la plupart des noyaux sont construits avec la gestion des 
disquettes.

<p>Pour monter une disquette au point de montage <tt>/floppy</tt>
(le r�pertoire devrait avoir �t� cr�� pendant l'installation), utilisez&nbsp;:
<list>
  <item><example>mount -t msdos /dev/fd0 /floppy/</example>
   Si la disquette est dans le lecteur A: et si son syst�me de fichiers est de type MS-DOS,
  <item><example>mount -t msdos /dev/fd1 /floppy/</example>
   Si la disquette est dans le lecteur B: et si son syst�me de fichiers est de type MS-DOS,
  <item><example>mount -t ext2 /dev/fd0 /floppy/</example>
   Si la disquette est dans le lecteur A: et si son syst�me de fichiers est de type ext2
   (le type normal pour Linux).
</list>

<sect id="remoteinstall">Puis-je installer directement depuis un site Internet distant&nbsp;?

<p>Oui. Vous pouvez d�marrer le syst�me d'installation de Debian depuis un ensemble de 
	fichiers que vous pouvez t�l�charger de notre site ftp ou de l'un de ses miroirs.

<p>Vous pouvez t�l�charger une petite image de c�d�rom, cr�er avec elle un c�d�rom amor�able,
installer le syst�me de base � partir de l'image puis finir l'installation � travers le r�seau.
Pour plus d'information, veuillez-vous reporter �
 <url id="http://www.debian.org/CD/netinst/">.

<p>Vous pouvez aussi t�l�charger les fichiers d'image de disquette encore plus petits, 
cr�er les disquettes amor�ables � partir d'elles, commencer la proc�dure d'installation 
et obtenir le reste de Debian depuis le r�seau.
Pour plus d'informations, veuillez-vous reporter �
<url id="http://www.debian.org/distrib/floppyinst">.
