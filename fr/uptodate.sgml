<!-- Subversion revision of original English document "3548" -->

<chapt id="uptodate">Garder son syst�me Debian � jour

<p>L'un des buts de Debian est de fournir un chemin de mise � jour compatible et un processus 
de mise � jour s�r. Nous faisons toujours de notre mieux pour que la mise � jour des nouvelles
versions soit une proc�dure douce. Dans certain cas, il peut y avoir des avertissements importants 
lors du processus de mise � jour, l'utilisateur en sera alert� et souvent une solution � un possible 
probl�me sera fournie.

<p>Vous pouvez aussi lire les notes de publication, qui d�crivent en d�tail les 
sp�cificit�s de la mise � jour, pr�sentes sur tous les c�d�roms de Debian et
disponibles sur le site web � l'adresse  
<url id="http://www.debian.org/releases/stable/releasenotes">.

<!-- ***************** PAS de TRADUCTION
<sect id="libc5to6upgrade">How can I upgrade my Debian 1.3.1 (or earlier)
  distribution, based on libc5, to 2.0 (or later), based on libc6?

<p>There are several ways to upgrade:
<list>
  <item>Using a simple shell script called <tt>autoup.sh</tt> which upgrades
    the most important packages. After <tt>autoup.sh</tt> has done his job,
    you may use dselect to install the remaining packages <em>en masse</em>.
    This is probably the recommended method, but not the only one.
    <p>Currently, the latest release of <tt>autoup.sh</tt> may be found on the
    following locations:
    <list>
      <item><url id="http://www.debian.org/releases/2.0/autoup/">
      <item><url id="http://www.taz.net.au/autoup/">
      <item><url id="http://debian.vicnet.net.au/autoup/">
    </list>
  <item>Following closely the <url name="Debian libc5 to libc6 Mini-HOWTO"
    id="http://debian.vicnet.net.au/autoup/HOWTO/libc5-libc6-Mini-HOWTO.html"> and
    upgrade the most important packages by hand. <tt>autoup.sh</tt> is based
    on this Mini-HOWTO, so this method should work more or less like using
    <tt>autoup.sh</tt>.
  <item>Using a libc5-based <tt>apt</tt>. APT stands for Advanced Package Tool, and
    it might replace dselect some day. Currently, it works just as a
    command-line interface, or as a dselect access method. You will find a
    libc5 version in the <tt>dists/slink/main/upgrade-older-i386</tt>
    directory at the Debian archives.
  <item>Using just dselect, without upgrading any package by hand
    first. It is highly recommended that you do NOT use this method
    if you can avoid it, because dselect alone currently does not install
    packages in the optimal order. APT works much better and it is safer.
    <!- - This should probably work if dpkg's max-error-before-stop internal
    variable is increased. Question: Will it be increased some day? - ->
</list>

<!- -
This paragraph is obsolete, but I will keep it here as a reminder in
case libc6-based dpkg happen to be some better than the one in Debian
1.3.1: Note that the version of <tt>dpkg</tt> in this directory has the
a.out binary format.  The versions of <tt>dpkg</tt> in the development and
stable trees have the ELF format.
-->


<sect id="howtocurrent">Comment puis-je garder mon syst�me � jour&nbsp;?

<p>On pourrait simplement ouvrir une session FTP anonyme vers une archive Debian, 
parcourir les r�pertoires jusqu'� ce qu'on trouve le fichier d�sir�, le r�cup�rer 
et enfin l'installer en utilisant <tt>dpkg</tt>. Notez que dpkg installera les fichiers 
mis � jour � leur place, m�me sur un syst�me en marche.
Parfois, la mise � jour d'un paquet aura besoin de l'installation d'une nouvelle version 
d'un autre paquet, auquel cas l'installation �chouera si l'autre paquet n'est pas install�.

<p>Beaucoup de gens trouvent cette approche trop gourmande en temps, car Debian �volue 
tr�s rapidement&nbsp;; typiquement, une douzaine ou plus de nouveaux paquets sont 
t�l�charg�s chaque semaine. Ce nombre est encore plus grand avant la sortie d'une version
majeure. Pour g�rer cette avalanche, beaucoup de gens pr�f�rent utiliser une m�thode 
automatique. Plusieurs outils de gestion des paquets sont disponibles dans ce but&nbsp;:

<sect1 id="apt">APT

<p>APT est une interface avanc�e pour le syst�me de gestion des paquets Debian. Apt-get est 
l'outil en ligne de commande pour la gestion des paquets et la m�thode APT de dselect est
une interface pour APT via <prgn/dselect/. Tous les deux fournissent une mani�re plus simple 
et plus s�re pour installer et mettre � jour les paquets.

<p>APT fournit les fonctionnalit�s d'ordonnancement d'installation
complet, de possibilit�s de sources multiples et plusieurs autres
fonctionnalit�s uniques, 
voir le guide de l'utilisateur
<tt>/usr/share/doc/apt-doc/guide.html/index.html</tt> (vous pouvez aussi installer
le paquet <tt>apt-doc</tt>).

<p>Installez le paquet <package/apt/ et �ditez le fichier <tt>/etc/apt/sources.list</tt> 
pour le configurer. Si vous souhaitez mettre � jour votre syst�me vers la derni�re version
stable de Debian, vous voudriez probablement utiliser une ligne comme celle-la

<example>http://http.us.debian.org/debian stable main contrib non-free</example>

<p>Vous pouvez remplacer http.us.debian.org avec le nom d'un miroir Debian plus
proche de chez vous et plus rapide. Voir la liste des miroirs � l'adresse 
<url id="http://www.debian.org/misc/README.mirrors"> pour plus d'informations.

<p>Vous trouverez plus d'informations en lisant les pages de manuel
<manref name="apt-get" section="8"> et <manref name="sources.list" section="8">,
ainsi que le guide de l'utilisateur d'APT mentionn� ci-dessus, 
<tt>/usr/share/doc/apt-doc/guide.html/index.html</tt>.

<p>Ex�cutez
      <example>apt-get update</example>
suivi par
      <example>apt-get dist-upgrade</example>
r�pondez � toutes les questions qui vous seront pos�es et votre syst�me sera mis � jour.

<p>Pour utiliser APT avec <prgn/dselect/, choisissez la m�thode d'acc�s par APT
dans l'�cran de s�lection de m�thode de dselect (option 0) et indiquez les sources
devant �tre utilis�es. Le fichier de configuration est <file>/etc/apt/sources.list</file>
et son format est d�crit dans la page de manuel <manref name="sources.list" section="5">.

<p>Si vous souhaitez utiliser les c�d�roms pour installer les paquets, vous 
pouvez utiliser le programme <prgn/apt-cdrom/. Pour plus de d�tails, veuillez-vous
reporter aux notes de publication, paragraphe �&nbsp;Ajouter des sources Internet � apt&nbsp;�.

<p>Veuillez noter que quand vous installez un paquet, les archives sont conserv�es dans 
un sous-r�pertoire de /var. Pour ne pas saturer votre partition, vous devriez supprimer
les archives en utilisant <tt>apt-get clean</tt> et <tt>apt-get autoclean</tt> ou les 
d�placer � un autre endroit (conseil&nbsp;: utiliser <package/apt-move/).

<sect1 id="dpkg-ftp">dpkg-ftp

<p>C'est une ancienne m�thode d'acc�s pour <prgn/dselect/. Il peut �tre appel� dans
<prgn/dselect/, laissant ainsi � un utilisateur la possibilit� de t�l�charger les
fichiers et de les installer directement en une �tape. Pour faire ceci, choisissez 
la m�thode d'acc�s <tt>ftp</tt> dans <prgn/dselect/ (option 0) et indiquez le nom 
d'h�te distant et le r�pertoire. <prgn/Dpkg-ftp/ t�l�chargera alors automatiquement 
les fichiers qui ont �t� choisis (soit dans cette session de <prgn/dselect/ ou
soit dans la pr�c�dente).

<p>Notez qu'� la diff�rence du programme <prgn/mirror/, <prgn/dpkg-ftp/ ne capture
pas tout le site miroir. Il t�l�charge seulement les fichiers que vous avez choisis 
(au premier lancement de <prgn/dpkg-ftp/) et qui ont besoin d'�tre mis � jour.

<p><prgn/dpkg-ftp/ est quelque peu obsol�te. Vous devriez � la place utiliser APT
avec des URLs ftp:// dans votre fichier <file>sources.list</file>.

<sect1 id="mirror">mirror

<p>Ce script Perl et son programme (optionel) de gestion nomm� <prgn/mirror-master/,
peuvent �tre employ�s pour rechercher des parties indiqu�es par l'utilisateur d'une
arborescence de r�pertoire sur un serveur particulier <em>via</em> un FTP anonyme.

<p><prgn/Mirror/ est particuli�rement utile pour t�l�charger un grand nombre
de logiciels. Apr�s le premier t�l�chargement, un fichier nomm� <tt>.mirrorinfo</tt>
est conserv� sur l'ordinateur local. Les changements du syst�me de fichiers distant
sont d�tect�s automatiquement par <prgn/mirror/, qui compare le fichier local avec 
un fichier semblable sur le syst�me distant et t�l�charge seulement les fichiers 
modifi�s.

<p>Le programme <prgn/mirror/ est g�n�ralement utile pour mettre � jour les copies 
locales des arborescences de r�pertoire distant. Les fichiers cherch�s n'ont pas besoin
d'�tre des fichiers Debian. (Depuis que le programmme <prgn/mirror/ a �t� port� en Perl, 
il peut aussi �tre ex�cut� sur des syst�mes non-Unix). Bien que le programme <prgn/mirror/ 
fournisse des m�canismes pour exclure des fichiers dont les noms correspondent � des cha�nes
indiqu�es par l'utilisateur, ce programme est plus utile quand son objectif est de t�l�charger 
des arborescences enti�res, plut�t que des paquets choisis.

<!-- Should we recommend GNU wget here, too? -->

<sect1 id="dpkg-mountable">dpkg-mountable

<p>Dpkg-mountable ajoute une m�thode d'acc�s nomm�e �&nbsp;mountable&nbsp;� � la liste de dselect, 
qui vous permet d'installer depuis tout syst�me de fichiers indiqu� dans /etc/fstab.
Par exemple, l'archive peut �tre sur un disque dur local ou sur un serveur NFS
qui sera automatiquement mont� et d�mont� si n�cessaire.

<p>Il a aussi quelques fonctionnalit�s suppl�mentaires que l'on ne trouve pas avec 
les m�thodes standards de dselect, telle que la disposition dans une arborescence
locale de fichier (soit en parall�le de la distribution principale soit totalement s�par�e),
et en obtenant seulement les paquets qui sont exig�s, plut�t qu'en effectuant un long balayage 
r�cursif de r�pertoires et �galement la journalisation de toutes les actions de dpkg dans 
la m�thode d'installation.

<sect id="upgradesingle">Faut-il �tre dans le mode mono-utilisateur pour mettre 
		� jour un paquet&nbsp;?

<p>Non. Les paquets peuvent �tre mis � jour en mode multi-utilisateur, m�me quand le syst�me
est en fonctionement. Il y a sur les syst�mes Debian le programme <tt>start-stop-daemon</tt>
qui permet d'arr�ter et de red�marrer les processus en cours d'ex�cution si cela est n�cessaire
pendant la mise � jour du paquet.

<sect id="savedebs">Faut-il garder toutes les achives .deb sur le disque&nbsp;?

<p>Non. Si vous avez t�l�charg� les fichiers sur votre disque (ce qui n'est absolument pas
n�cessaire, voir au-dessus pour la description de dpkg-ftp), alors apr�s 
avoir install� les paquets vous pouvez les supprimer de votre syst�me.

<sect id="keepingalog">Comment puis-je garder un journal des paquets que j'ai ajout� 
				sur le syst�me&nbsp;

<p><prgn/Dpkg/ garde une trace des paquets que vous avez configur�s, supprim�s et/ou purg�s
mais ne garde pas (actuellement) de journal sur l'affichage du terminal qui a lieu lors 
de la manipulation d'un paquet.

<p>Le moyen le plus simple pour contourner ce probl�me est de lancer vos sessions
<prgn>dpkg</prgn>/<prgn>dselect</prgn>/<prgn>apt-get</prgn> avec le programme
<manref name="script" section="1">.
